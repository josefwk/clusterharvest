local self = require('openmw.self') 
local nearby = require('openmw.nearby')
local types = require('openmw.types')

--[[
    Note on performance and implementation: this performs N*M operations.
    N is the number of containers to query, and M is the number of valid containers activated by the end of the operation.
    For example, if there are 900 containers N, and 12 valid containers M that get activated,
    each of those 12 containers will iterate through the set of 900 containers, for a total of 10,800 calls.

    When querying containers, a naive approach would use "nearby.containers" as N, which is a relatively large set.
    Therefore, we should reduce the set of containers N where possible.
    The current API cannot get a set of objects at distance D from some point P, so we start with nearby.containers.
    However, we should immediately remove all invalid containers from that set.
    For example, we only want a set F with containers matching the display name of the first activated container.
    Thus, we perform N+F*M operations, where F <= N (almost certainly smaller than N in practice).

    So, using the example above, even if 450 of the 900 containers in N were valid for set F, 
    we would only have (900+450*12) calls, or 6,300 calls, rather than 10,800.
]]

--[[ 
    Given two matching harvestable containers, upon activating one, 
    this is the max distance that they will both be valid to harvest.
    Modify this to adjust overall distance of cluster harvesting behavior.
]]
maxDistanceHarvestable = 400
matchingContainerSet = {}
visitedNodes = {}
startingNode = nil
lastNodeAdded = nil

function addToSet(set, key)
    set[key] = true
end

function setContains(set, key)
    return set[key] ~= nil
end

function starts_with(str, start)
    return str:sub(1, #start) == start
end

local containersToIgnore = {
    ["flora_treestump_unique"] = true,
    ["flora_bittergreen_pod_01"] = true,
    ["flora_bittergreen_08"] = true,
    ["flora_bittergreen_09"] = true,
    ["flora_bittergreen_10"] = true,
}

local validContainers = {
    ["flora_"] = true,
    ["kollop"] = true,
    ["egg_"] = true,
    ["tramaroot_"] = true,
}

function isValidMatchingContainer(container, displayNameToMatch)

    -- only consider containers with identical display names (not record ID, as this can vary (e.g. kollop_01, kollop_02))
    containerName = types.Container.record(container).name
    if displayNameToMatch ~= containerName then return false end

    -- check if this recordId should be ignored (e.g. due to activating an inventory screen, even with Graphic Herbalism)
    if containersToIgnore[container.recordId] then return false end

    -- is this recordId in the list of valid containers (e.g. is it a crate, or is it flora?)
    for validContainerNamePrefix, i in pairs(validContainers) do
        if starts_with(container.recordId, validContainerNamePrefix) then return true end
    end

    -- TODO: Reject harvestable containers based on ownership to avoid inadvertant criminal acts (0.49, API_REVISION 53+)

    return false 
end

function isWithinValidDistance(sourceNode, targetNode)
    -- only consider containers within a certain distance from the container this query originated from
    dist = (sourceNode.position - targetNode.position):length()
    return dist <= maxDistanceHarvestable 
end

function attemptContainerGrab(sourceNode, targetNode) 
    -- ignore nil and self-references
    if sourceNode == targetNode or targetNode == nil then return false end
    
    if isWithinValidDistance(sourceNode, targetNode) == false then return end

    -- add the target node to the list of visited nodes before activating other containers (avoid cyclic references)
    addToSet(visitedNodes, tostring(targetNode))
    
    -- track the last node visited to determine when to free the list of visited nodes at the end of the query chain
    lastNodeAdded = targetNode

    -- TODO: Examine alternative method for taking items from containers without Graphic Herbalism
    targetNode:activateBy(self)
end

function handleActivation(sourceContainer)
    -- if this is the initial interaction, filter set of nearby containers to valid subset for subsequent queries
    if startingNode == nil then
        startingNode = sourceContainer
        displayNameToMatch = types.Container.record(sourceContainer).name

        for i, container in pairs(nearby.containers) do
            if isValidMatchingContainer(container, displayNameToMatch) then
                matchingContainerSet[tostring(container)] = container
            end
        end
    end

    lastNodeAdded = startingNode
    
    -- query valid subset to see if it is a harvestable target relative to the source container
    for i, targetContainer in pairs(matchingContainerSet) do
        if (setContains(visitedNodes, tostring(targetContainer)) == false) then
            attemptContainerGrab(sourceContainer, targetContainer)
        end
    end

    if sourceContainer == lastNodeAdded then
        matchingContainerSet = {}
        visitedNodes = {}
        startingNode = nil
        lastNodeAdded = nil
    end

end

return {
    eventHandlers = {
        OnContainerOpened = handleActivation,
    }
}
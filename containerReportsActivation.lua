local self = require('openmw.self')

local function handleActivation(actor) 
    actor:sendEvent("OnContainerOpened", self.object)
end

return {
    engineHandlers = {
        onActivated = handleActivation
    }
}
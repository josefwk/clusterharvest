# Cluster Harvest Mod for OpenMW 0.48+

## Overview

This mod allows you to harvest from all nearby ingredients of the same type that you select. For example, attempting to harvest a Stoneflower Petal will also harvest from all nearby Stoneflower Petal plants, but not any other type of plant. The distance is kept relatively small to prevent grabbing ingredients at an unreasonable distance, but you can adjust this distance limit in the `clusterHarvest.lua` script if you wish.

## Requirements

Graphic Herbalism must be enabled.

## Disclaimer

This is a work in progress and in no way has it been thoroughly tested for public consumption. Use at your own risk.